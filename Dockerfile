FROM rust:1.73

RUN apt update && apt install -y libusb-1.0-0-dev libudev-dev
RUN rustup target add thumbv6m-none-eabi
RUN rustup component add clippy-preview
RUN rustup component add rustfmt
RUN cargo install flip-link elf2uf2-rs
RUN rustc --version && cargo --version
