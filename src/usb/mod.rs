use rp_pico::hal::usb;

use rp_pico::hal;

use usb_device::{class_prelude::*, prelude::*};

use usbd_serial::{SerialPort, USB_CLASS_CDC};

mod winusb;

use winusb::MicrosoftDescriptors;

static mut BUS: Option<UsbBusAllocator<usb::UsbBus>> = None;

pub struct Usb {
    winusb: Option<MicrosoftDescriptors>,
    usb_serialm: Option<SerialPort<'static, hal::usb::UsbBus>>,
    usb_serial1: Option<SerialPort<'static, hal::usb::UsbBus>>,
    usb_serial2: Option<SerialPort<'static, hal::usb::UsbBus>>,
    usb_device: Option<UsbDevice<'static, hal::usb::UsbBus>>,
}

impl Usb {
    pub fn new(usb_bus: UsbBusAllocator<usb::UsbBus>) -> Self {
        unsafe {
            BUS = Some(usb_bus);
        }

        Usb {
            winusb: Some(MicrosoftDescriptors),
            usb_serialm: Some(SerialPort::new(unsafe { BUS.as_ref().unwrap() })),
            usb_serial1: Some(SerialPort::new(unsafe { BUS.as_ref().unwrap() })),
            usb_serial2: Some(SerialPort::new(unsafe { BUS.as_ref().unwrap() })),
            usb_device: Some(
                UsbDeviceBuilder::new(unsafe { BUS.as_ref().unwrap() }, UsbVidPid(0x6666, 0x4444))
                    .manufacturer("Test")
                    .composite_with_iads()
                    .product("Test-control-board-m1")
                    .serial_number("TEST")
                    .device_class(USB_CLASS_CDC)
                    .max_power(500)
                    .build(),
            ),
        }
    }

    pub fn process_usb(
        &mut self,
        usb_serialm: &mut SerialPort<hal::usb::UsbBus>,
        usb_serial1: &mut SerialPort<hal::usb::UsbBus>,
        usb_serial2: &mut SerialPort<hal::usb::UsbBus>,
    ) -> bool {
        let usb_device = self.usb_device.as_mut().unwrap();
        let winusb = self.winusb.as_mut().unwrap();

        usb_device.poll(&mut [winusb, usb_serialm, usb_serial1, usb_serial2])
    }

    pub fn get_usb_uartm(&mut self) -> SerialPort<'static, hal::usb::UsbBus> {
        self.usb_serialm.take().unwrap()
    }

    pub fn get_usb_uart1(&mut self) -> SerialPort<'static, hal::usb::UsbBus> {
        self.usb_serial1.take().unwrap()
    }

    pub fn get_usb_uart2(&mut self) -> SerialPort<'static, hal::usb::UsbBus> {
        self.usb_serial2.take().unwrap()
    }
}

// End of file
