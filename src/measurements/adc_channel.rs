use core::cell::RefCell;
use embedded_hal::digital::v2::OutputPin;

pub enum AdcChannels {
    HighV,
    HighCur,
    _UsbCc1,
    _UsbCc2,
    UsbCur,
}

pub struct AdcChannel {
    channel: AdcChannels,
    ratio: f32,
    filter_data: RefCell<u32>,
    filter_empty: RefCell<bool>,
}

const SAMPLE_COUNT: u32 = 10;

impl AdcChannel {
    pub fn new(channel: AdcChannels, ratio: f32) -> Self {
        AdcChannel {
            channel,
            ratio,
            filter_data: RefCell::new(0),
            filter_empty: RefCell::new(true),
        }
    }

    pub fn read(&self) -> f32 {
        let filter_data = self.filter_data.borrow();
        ((*filter_data / SAMPLE_COUNT) as f32) / self.ratio
    }

    pub fn process(&self, adccombo: &mut crate::measurements::adc_combo::StructAdcMeas) {
        let val: u16 = match self.channel {
            AdcChannels::HighV => {
                adccombo.pin_high_iv_select.set_low().unwrap();
                cortex_m::asm::delay(1000);
                adccombo.read0()
            }
            AdcChannels::HighCur => {
                adccombo.pin_high_iv_select.set_high().unwrap();
                cortex_m::asm::delay(1000);
                adccombo.read0()
            }
            AdcChannels::_UsbCc1 => {
                adccombo.pin_usb_cc_select.set_low().unwrap();
                cortex_m::asm::delay(1000);
                adccombo.read1()
            }
            AdcChannels::_UsbCc2 => {
                adccombo.pin_usb_cc_select.set_high().unwrap();
                cortex_m::asm::delay(1000);
                adccombo.read1()
            }
            AdcChannels::UsbCur => adccombo.read2(),
        };

        let mut filter_data = self.filter_data.borrow_mut();

        if *self.filter_empty.borrow_mut() {
            *self.filter_empty.borrow_mut() = false;
            *filter_data = (val as u32) * SAMPLE_COUNT;
        } else {
            let mean = *filter_data / SAMPLE_COUNT;
            *filter_data -= mean;
            *filter_data += val as u32;
        }
    }
}
