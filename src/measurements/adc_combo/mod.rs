use core::borrow::BorrowMut;
use cortex_m::prelude::_embedded_hal_adc_OneShot;
use embedded_hal::digital::v2::OutputPin;
use rp_pico::hal::{
    gpio::{bank0, Floating, Input, Output, Pin, PushPull},
    Adc,
};

pub type PinAdc0 = Pin<bank0::Gpio26, Input<Floating>>;
pub type PinAdc1 = Pin<bank0::Gpio27, Input<Floating>>;
pub type PinAdc2 = Pin<bank0::Gpio28, Input<Floating>>;
pub type _PinAdcT = Pin<bank0::Gpio29, Input<Floating>>;
pub type PinHighVoltageIVMeasSelect = Pin<bank0::Gpio21, Output<PushPull>>;

pub type PinUsbVMeasSelect = Pin<bank0::Gpio7, Output<PushPull>>;

pub type StructAdcMeas =
    AdcMeas<PinAdc0, PinAdc1, PinAdc2, PinHighVoltageIVMeasSelect, PinUsbVMeasSelect>;

pub struct AdcMeas<A, B, C, S1, S2> {
    pub pin_high_v_adc: A,
    pub pin_usb_cc_adc: B,
    pub pin_usb_i_adc: C,
    pub adc: Adc,
    pub pin_high_iv_select: S1,
    pub pin_usb_cc_select: S2,
}

impl<
        A: embedded_hal::adc::Channel<rp_pico::hal::Adc, ID = u8>,
        B: embedded_hal::adc::Channel<rp_pico::hal::Adc, ID = u8>,
        C: embedded_hal::adc::Channel<rp_pico::hal::Adc, ID = u8>,
        S1: OutputPin,
        S2: OutputPin,
    > AdcMeas<A, B, C, S1, S2>
{
    pub fn new(
        pin_high_v_adc: A,
        pin_usb_cc_adc: B,
        pin_usb_i_adc: C,
        adc: Adc,
        pin_high_iv_select: S1,
        pin_usb_cc_select: S2,
    ) -> Self {
        AdcMeas {
            pin_high_v_adc,
            pin_usb_cc_adc,
            pin_usb_i_adc,
            adc,
            pin_high_iv_select,
            pin_usb_cc_select,
        }
    }

    pub fn read0(&mut self) -> u16 {
        let result: u16 = self
            .adc
            .borrow_mut()
            .read(&mut self.pin_high_v_adc)
            .unwrap();

        result
    }
    pub fn read1(&mut self) -> u16 {
        let result: u16 = self
            .adc
            .borrow_mut()
            .read(&mut self.pin_usb_cc_adc)
            .unwrap();

        result
    }
    pub fn read2(&mut self) -> u16 {
        let result: u16 = self.adc.borrow_mut().read(&mut self.pin_usb_i_adc).unwrap();

        result
    }
}
