use defmt::Format;

#[derive(PartialEq, Clone)]
pub struct RelayState {
    pub state: bool,
}

#[derive(PartialEq, Clone)]
pub struct PowerState {
    pub state: bool,
    pub voltage: f32,
    pub current: f32,
}

#[derive(PartialEq, Clone, Format)]
pub enum ControlSource {
    Manual = 0,
    Modbus = 1,
}

#[derive(PartialEq, Clone)]
pub struct BoardState {
    pub acs: ControlSource,

    pub relay1: RelayState,
    pub relay2: RelayState,
    pub relay3: RelayState,
    pub relay4: RelayState,

    pub high_v: PowerState,
    pub usb: PowerState,
    pub in_voltage: f32,
    pub in_voltage_ok: bool,

    pub modbus_requested_page: u8,
    pub modbus_requested_acs: u8,
}

impl BoardState {
    pub fn new() -> BoardState {
        BoardState {
            acs: ControlSource::Modbus,
            relay1: RelayState { state: false },
            relay2: RelayState { state: false },
            relay3: RelayState { state: false },
            relay4: RelayState { state: false },
            high_v: PowerState {
                state: false,
                voltage: 0.0,
                current: 0.0,
            },
            usb: PowerState {
                state: false,
                voltage: 0.0,
                current: 0.0,
            },
            in_voltage: 0.0,
            in_voltage_ok: false,
            modbus_requested_page: 0,
            modbus_requested_acs: 0,
        }
    }
}

impl Default for BoardState {
    fn default() -> Self {
        Self::new()
    }
}
