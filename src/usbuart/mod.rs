use rp_pico::hal;

pub fn usb_uart_tx_handler(
    consumer: &mut heapless::spsc::Consumer<u8, 512>,
    usb_uart: &mut usbd_serial::SerialPort<'static, hal::usb::UsbBus>,
) {
    while let Some(data) = consumer.peek() {
        let d: [u8; 1] = [*data];

        // below code does following:
        // try to write byte, and no matter of result drop data.
        // I found no other way of ensuring someone is actually connected to USB,
        // This dirty way we ensure data that cannot be sent are discarded.
        // It also give possibility that we lose data in case that USB buffer is full at the moment..
        // No better idea at the moment.

        usb_uart.write(&d).ok();
        _ = consumer.dequeue();
    }
    usb_uart.flush().ok();
}

pub fn usb_uart_rx_handler(
    producer: &mut heapless::spsc::Producer<u8, 512>,
    usb_uart: &mut usbd_serial::SerialPort<'static, hal::usb::UsbBus>,
) {
    let mut space = (producer.capacity() - producer.len()) / 16;

    let mut received = [0; 16];

    while space > 0 {
        match usb_uart.read(&mut received) {
            Err(usb_device::UsbError::WouldBlock) => {
                // Do nothing
                break;
            }
            Err(_e) => {
                // Do nothing
                defmt::println!("process_usb_uart1: receive error");
                break;
            }
            Ok(0) => {
                // Do nothing
                break;
            }
            Ok(count) => {
                let mut i = 0;
                while count > i {
                    producer.enqueue(received[i]).ok();
                    i += 1;
                    space -= 0;
                }
            }
        }
    }
}
