use core::cell::RefCell;

use embedded_hal::digital::v2::OutputPin;
use fugit::RateExtU32;

use hal::gpio::Uart;
use hal::Clock;
use rp_pico::hal::pac;
use rp_pico::hal::{self};
use ssd1306::{prelude::*, Ssd1306};

use rp_pico::hal::gpio::{
    bank0, Floating, FloatingInput, Function, FunctionI2C, FunctionUart, Input, Output, Pin,
    PushPull, PushPullOutput, I2C,
};

use crate::measurements::adc_combo::StructAdcMeas;

use usb_device::class_prelude::*;

type BoardI2c = rp_pico::hal::I2C<
    pac::I2C1,
    (
        Pin<bank0::Gpio18, Function<I2C>>,
        Pin<bank0::Gpio19, Function<I2C>>,
    ),
>;

pub type PinRelay0 = Pin<bank0::Gpio9, Output<PushPull>>;
pub type PinRelay1 = Pin<bank0::Gpio11, Output<PushPull>>;
pub type PinRelay2 = Pin<bank0::Gpio13, Output<PushPull>>;
pub type PinRelay3 = Pin<bank0::Gpio15, Output<PushPull>>;

pub type PinButton1 = Pin<bank0::Gpio8, Input<Floating>>;
pub type PinButton2 = Pin<bank0::Gpio10, Input<Floating>>;
pub type PinButton3 = Pin<bank0::Gpio12, Input<Floating>>;
pub type PinButton4 = Pin<bank0::Gpio14, Input<Floating>>;
pub type PinButton5 = Pin<bank0::Gpio16, Input<Floating>>;
pub type PinButton6 = Pin<bank0::Gpio2, Input<Floating>>;
pub type PinButton7 = Pin<bank0::Gpio22, Input<Floating>>;
pub type PinPicoLed = Pin<bank0::Gpio25, Output<PushPull>>;
pub type PinErrorLed = Pin<bank0::Gpio17, Output<PushPull>>;

pub type PinHighVoltageOnOff = Pin<bank0::Gpio20, Output<PushPull>>;

pub type PinUsbHostDetect = Pin<bank0::Gpio3, Output<PushPull>>;
pub type PinUsbOnOff = Pin<bank0::Gpio6, Output<PushPull>>;

pub type Uart0Pins = (
    Pin<bank0::Gpio0, Function<Uart>>,
    Pin<bank0::Gpio1, Function<Uart>>,
);

pub type Uart1Pins = (
    Pin<bank0::Gpio4, Function<Uart>>,
    Pin<bank0::Gpio5, Function<Uart>>,
);

pub type Uart0 = hal::uart::UartPeripheral<hal::uart::Disabled, pac::UART0, Uart0Pins>;
pub type Uart1 = hal::uart::UartPeripheral<hal::uart::Disabled, pac::UART1, Uart1Pins>;

pub type Display = Ssd1306<
    I2CInterface<BoardI2c>,
    ssd1306::size::DisplaySize128x32,
    ssd1306::mode::BufferedGraphicsMode<ssd1306::size::DisplaySize128x32>,
>;

pub struct Board {
    timer: Option<hal::Timer>,
    display: Option<Display>,

    relay0: Option<PinRelay0>,

    relay1: Option<PinRelay1>,

    relay2: Option<PinRelay2>,

    relay3: Option<PinRelay3>,

    button1: Option<PinButton1>,

    button2: Option<PinButton2>,

    button3: Option<PinButton3>,

    button4: Option<PinButton4>,

    button5: Option<PinButton5>,

    button6: Option<PinButton6>,

    button7: Option<PinButton7>,

    high_voltage_on_off: Option<PinHighVoltageOnOff>,

    _usb_host_detect: Option<PinUsbHostDetect>,
    usb_on_off: Option<PinUsbOnOff>,

    adcmeas: RefCell<Option<StructAdcMeas>>,

    usb_bus: RefCell<Option<UsbBusAllocator<hal::usb::UsbBus>>>,
    pico_led: Option<PinPicoLed>,
    error_led: Option<PinErrorLed>,
    // uart1: Option<Uart0>,
    // uart2: Option<Uart1>,
    uart1_pins: Option<Uart0Pins>,
    uart1_pac: Option<rp_pico::hal::pac::UART0>,

    uart2_pins: Option<Uart1Pins>,
    uart2_pac: Option<rp_pico::hal::pac::UART1>,
    resets: rp_pico::hal::pac::RESETS,
    freq: fugit::HertzU32,
}

impl Board {
    pub fn new(mut pac: rp_pico::hal::pac::Peripherals) -> Self {
        unsafe {
            hal::sio::spinlock_reset();
        }

        let sio = hal::Sio::new(pac.SIO);
        let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

        let clocks = hal::clocks::init_clocks_and_plls(
            rp_pico::XOSC_CRYSTAL_FREQ,
            pac.XOSC,
            pac.CLOCKS,
            pac.PLL_SYS,
            pac.PLL_USB,
            &mut pac.RESETS,
            &mut watchdog,
        )
        .ok()
        .unwrap();

        let pins = rp_pico::Pins::new(
            pac.IO_BANK0,
            pac.PADS_BANK0,
            sio.gpio_bank0,
            &mut pac.RESETS,
        );

        let uart0_txd = pins.gpio0.into_mode::<FunctionUart>();
        let uart0_rxd = pins.gpio1.into_mode::<FunctionUart>();

        let uart1_txd = pins.gpio4.into_mode::<FunctionUart>();
        let uart1_rxd = pins.gpio5.into_mode::<FunctionUart>();

        let sda_pin = pins.gpio18.into_mode::<FunctionI2C>();
        let scl_pin = pins.gpio19.into_mode::<FunctionI2C>();

        let pin_adc0 = pins.gpio26.into_mode::<FloatingInput>();
        let pin_adc1 = pins.gpio27.into_mode::<FloatingInput>();
        let pin_adc2 = pins.gpio28.into_mode::<FloatingInput>();

        let pin_high_iv_select = pins.gpio21.into_mode::<PushPullOutput>();
        let pin_usb_cc_select = pins.gpio7.into_mode::<PushPullOutput>();

        let adc = hal::Adc::new(pac.ADC, &mut pac.RESETS);

        let peripheral_clock = &clocks.peripheral_clock;

        let display = Ssd1306::new(
            ssd1306::I2CDisplayInterface::new(Self::create_i2c(
                pac.I2C1,
                &mut pac.RESETS,
                sda_pin,
                scl_pin,
                peripheral_clock,
            )),
            DisplaySize128x32,
            DisplayRotation::Rotate180,
        )
        .into_buffered_graphics_mode();

        let usb_bus = hal::usb::UsbBus::new(
            pac.USBCTRL_REGS,
            pac.USBCTRL_DPRAM,
            clocks.usb_clock,
            true,
            &mut pac.RESETS,
        );
        let usb_bus_alloc1 = UsbBusAllocator::new(usb_bus);

        let mut error_led = pins.gpio17.into_mode::<PushPullOutput>();
        error_led.set_high().unwrap();

        Board {
            timer: Some(hal::Timer::new(pac.TIMER, &mut pac.RESETS)),
            display: Some(display),
            relay0: Some(pins.gpio9.into_mode::<PushPullOutput>()),
            relay1: Some(pins.gpio11.into_mode::<PushPullOutput>()),
            relay2: Some(pins.gpio13.into_mode::<PushPullOutput>()),
            relay3: Some(pins.gpio15.into_mode::<PushPullOutput>()),
            button1: Some(pins.gpio8.into_mode::<FloatingInput>()),
            button2: Some(pins.gpio10.into_mode::<FloatingInput>()),
            button3: Some(pins.gpio12.into_mode::<FloatingInput>()),
            button4: Some(pins.gpio14.into_mode::<FloatingInput>()),
            button5: Some(pins.gpio16.into_mode::<FloatingInput>()),
            button6: Some(pins.gpio2.into_mode::<FloatingInput>()),
            button7: Some(pins.gpio22.into_mode::<FloatingInput>()),

            high_voltage_on_off: Some(pins.gpio20.into_mode::<PushPullOutput>()),
            _usb_host_detect: Some(pins.gpio3.into_mode::<PushPullOutput>()),
            usb_on_off: Some(pins.gpio6.into_mode::<PushPullOutput>()),

            adcmeas: RefCell::new(Some(StructAdcMeas::new(
                pin_adc0,
                pin_adc1,
                pin_adc2,
                adc,
                pin_high_iv_select,
                pin_usb_cc_select,
            ))),
            usb_bus: RefCell::new(Some(usb_bus_alloc1)),
            pico_led: Some(pins.led.into_mode::<PushPullOutput>()),
            error_led: Some(error_led),
            // uart1: Some(
            //     hal::uart::UartPeripheral::new(pac.UART0, (uart0_txd, uart0_rxd), &mut pac.RESETS)
            //         .enable(
            //             UartConfig::new(115200.Hz(), DataBits::Eight, None, StopBits::One),
            //             clocks.peripheral_clock.freq(),
            //         )
            //         .unwrap(),
            // ),
            // uart2: Some(
            //     hal::uart::UartPeripheral::new(pac.UART1, (uart1_txd, uart1_rxd), &mut pac.RESETS)
            //         .enable(
            //             UartConfig::new(115200.Hz(), DataBits::Eight, None, StopBits::One),
            //             clocks.peripheral_clock.freq(),
            //         )
            //         .unwrap(),
            // ),
            uart1_pins: Some((uart0_txd, uart0_rxd)),
            uart1_pac: Some(pac.UART0),

            uart2_pins: Some((uart1_txd, uart1_rxd)),
            uart2_pac: Some(pac.UART1),
            resets: pac.RESETS,
            freq: clocks.peripheral_clock.freq(),
        }
    }

    fn create_i2c(
        paci2c1: rp_pico::hal::pac::I2C1,
        resets: &mut pac::RESETS,
        sdapin: Pin<bank0::Gpio18, Function<I2C>>,
        sclpin: Pin<bank0::Gpio19, Function<I2C>>,
        peripheral_clock: &rp_pico::hal::clocks::PeripheralClock,
    ) -> BoardI2c {
        hal::I2C::i2c1(paci2c1, sdapin, sclpin, 400.kHz(), resets, peripheral_clock)
    }
    pub fn get_timer(&mut self) -> hal::Timer {
        self.timer.take().unwrap()
    }

    pub fn get_display(
        &mut self,
    ) -> Ssd1306<
        I2CInterface<BoardI2c>,
        ssd1306::size::DisplaySize128x32,
        ssd1306::mode::BufferedGraphicsMode<ssd1306::size::DisplaySize128x32>,
    > {
        self.display.take().unwrap()
    }

    pub fn get_relay0(&mut self) -> PinRelay0 {
        self.relay0.take().unwrap()
    }

    pub fn get_relay1(&mut self) -> PinRelay1 {
        self.relay1.take().unwrap()
    }

    pub fn get_relay2(&mut self) -> PinRelay2 {
        self.relay2.take().unwrap()
    }

    pub fn get_relay3(&mut self) -> PinRelay3 {
        self.relay3.take().unwrap()
    }

    pub fn get_high_v_on_off(&mut self) -> PinHighVoltageOnOff {
        self.high_voltage_on_off.take().unwrap()
    }

    pub fn get_usb_on_off(&mut self) -> PinUsbOnOff {
        self.usb_on_off.take().unwrap()
    }

    pub fn get_button1(&mut self) -> PinButton1 {
        self.button1.take().unwrap()
    }

    pub fn get_button2(&mut self) -> PinButton2 {
        self.button2.take().unwrap()
    }

    pub fn get_button3(&mut self) -> PinButton3 {
        self.button3.take().unwrap()
    }

    pub fn get_button4(&mut self) -> PinButton4 {
        self.button4.take().unwrap()
    }

    pub fn get_button5(&mut self) -> PinButton5 {
        self.button5.take().unwrap()
    }

    pub fn get_button6(&mut self) -> PinButton6 {
        self.button6.take().unwrap()
    }

    pub fn get_button7(&mut self) -> PinButton7 {
        self.button7.take().unwrap()
    }

    // pub fn get_high_v_meas(&self) -> AdcChannel {
    //     AdcChannel::new(&self.adcmeas, AdcChannels::HighV, 125.0)
    // }
    // pub fn get_high_i_meas(&self) -> AdcChannel {
    //     AdcChannel::new(&self.adcmeas, AdcChannels::HighCur, 650.0)
    // }
    // pub fn get_usb_i_meas(&self) -> AdcChannel {
    //     AdcChannel::new(&self.adcmeas, AdcChannels::UsbCur, 650.0)
    // }

    pub fn get_adc_combo(&self) -> StructAdcMeas {
        self.adcmeas.take().unwrap()
    }

    pub fn get_usb_bus(&self) -> UsbBusAllocator<hal::usb::UsbBus> {
        self.usb_bus.take().unwrap()
    }
    pub fn get_pico_led(&mut self) -> PinPicoLed {
        self.pico_led.take().unwrap()
    }

    #[allow(dead_code)]
    pub fn get_error_led(&mut self) -> PinErrorLed {
        self.error_led.take().unwrap()
    }

    // pub fn get_uart1(&mut self) -> Uart0 {
    //     self.uart1.take().unwrap()
    // }
    // pub fn get_uart2(&mut self) -> Uart1 {
    //     self.uart2.take().unwrap()
    // }

    pub fn get_uart1(&mut self) -> Uart0 {
        hal::uart::UartPeripheral::new(
            self.uart1_pac.take().unwrap(),
            self.uart1_pins.take().unwrap(),
            &mut self.resets,
        )
    }

    pub fn get_uart2(&mut self) -> Uart1 {
        hal::uart::UartPeripheral::new(
            self.uart2_pac.take().unwrap(),
            self.uart2_pins.take().unwrap(),
            &mut self.resets,
        )
    }

    pub fn get_uarts_freq(&self) -> fugit::HertzU32 {
        self.freq
    }
}
