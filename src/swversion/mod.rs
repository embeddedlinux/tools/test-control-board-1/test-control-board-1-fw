#[allow(dead_code)]
pub fn get_version() -> &'static str {
    env!("CARGO_PKG_VERSION")
}

#[allow(dead_code)]
pub fn get_major() -> u16 {
    let version_major: u16 = env!("CARGO_PKG_VERSION_MAJOR").trim().parse().unwrap();
    version_major
}

#[allow(dead_code)]
pub fn get_minor() -> u16 {
    let version_minor: u16 = env!("CARGO_PKG_VERSION_MINOR").trim().parse().unwrap();
    version_minor
}

#[allow(dead_code)]
pub fn get_patch() -> u16 {
    let version_patch: u16 = env!("CARGO_PKG_VERSION_PATCH").trim().parse().unwrap();
    version_patch
}
