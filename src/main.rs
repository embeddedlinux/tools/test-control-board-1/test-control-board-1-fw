#![no_std]
#![no_main]

use defmt::println;
use panic_halt as _;

mod board;
mod button;
mod measurements;
mod modbus;
mod relay;
mod state;
mod swversion;
mod uart;
mod ui;
mod usb;
mod usbuart;

const IN_VOLTAGE_MINIMUM: f32 = 8.8;
const SHOW_VALUES_WHEN_OFF: bool = false;

const CURRENT_MULT: f32 = 100.0;
const VOLTAGE_MULT: f32 = 10.0;

use fugit::RateExtU32;
use rp_pico::hal;

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct UsbUartParams {
    pub stop_bits: usbd_serial::StopBits,
    pub data_bits: u8,
    pub parity_type: usbd_serial::ParityType,
    pub data_rate: u32,
}

impl core::fmt::Debug for UsbUartParams {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "Hi: {}", self.data_rate)
    }
}

impl UsbUartParams {
    pub fn new(
        stop_bits: usbd_serial::StopBits,
        data_bits: u8,
        parity_type: usbd_serial::ParityType,
        data_rate: u32,
    ) -> Self {
        Self {
            stop_bits,
            data_bits,
            parity_type,
            data_rate,
        }
    }

    pub fn to_uart_params(&mut self) -> hal::uart::UartConfig {
        let mut data_bits: hal::uart::DataBits = hal::uart::DataBits::Eight;
        match self.data_bits {
            5 => data_bits = hal::uart::DataBits::Five,
            6 => data_bits = hal::uart::DataBits::Six,
            7 => data_bits = hal::uart::DataBits::Seven,
            8 => data_bits = hal::uart::DataBits::Eight,
            _ => println!("unsupported data bits: {}", self.data_bits),
        }

        let mut parity_bits: Option<hal::uart::Parity> = None;
        match self.parity_type {
            usbd_serial::ParityType::None => parity_bits = None,
            usbd_serial::ParityType::Odd => parity_bits = Some(hal::uart::Parity::Odd),
            usbd_serial::ParityType::Event => parity_bits = Some(hal::uart::Parity::Even),
            _ => println!("unsupported parity bits: {}", self.data_bits),
        }

        let mut stop_bits: hal::uart::StopBits = hal::uart::StopBits::One;
        match self.stop_bits {
            usbd_serial::StopBits::One => stop_bits = hal::uart::StopBits::One,
            usbd_serial::StopBits::Two => stop_bits = hal::uart::StopBits::Two,
            _ => println!("unsupported stop bits: {}", self.data_bits),
        }
        hal::uart::UartConfig::new(self.data_rate.Hz(), data_bits, parity_bits, stop_bits)
    }
}

#[rtic::app(device = rp_pico::hal::pac, peripherals = true, dispatchers = [TIMER_IRQ_1, TIMER_IRQ_2, TIMER_IRQ_3])]
mod app {

    use crate::board::{
        Board, PinButton1, PinButton2, PinButton3, PinButton4, PinButton5, PinButton6, PinButton7,
        PinHighVoltageOnOff, PinPicoLed, PinRelay0, PinRelay1, PinRelay2, PinRelay3, PinUsbOnOff,
    };
    use crate::relay::{IRelay, Relay};
    use crate::state::ControlSource;
    use crate::uart::*;
    use crate::ui::Ui;
    use crate::usbuart::*;
    use crate::{CURRENT_MULT, VOLTAGE_MULT};
    use defmt::println;
    use embedded_hal::digital::v2::OutputPin;
    use fugit::ExtU64;

    // use hal::pwm::B;

    use crate::measurements::adc_channel::{AdcChannel, AdcChannels};
    use rp2040_hal::timer::monotonic::Monotonic;
    use rp_pico::hal::timer::Alarm0;

    use crate::state::BoardState;
    use crate::usb::*;

    use fixedvec::{alloc_stack, FixedVec};
    use rp_pico::hal;

    use heapless::spsc::{Consumer, Producer, Queue};

    use fugit::RateExtU32;
    use hal::uart::{DataBits, StopBits, UartConfig, UartPeripheral};

    use rp2040_hal::pac::{UART0, UART1};

    use crate::modbus::{
        Modbus, DEV_ACS_IR_ADDRESS, DEV_ACS_TAKE_COIL_ADDRESS, DEV_INPUT_VOLTAGE_IR_ADDRESS,
        DEV_SCREEN_COUNT_IR_ADDRESS, DEV_SCREEN_PAGE_HR_ADDRESS, DEV_SCREEN_PAGE_IR_ADDRESS,
        J9_COIL_ADDRESS, J9_DI_STATE_ADDRESS, J9_IR_CURRENT_ADDRESS, J9_IR_VOLTAGE_ADDRESS,
        RELAY0_COIL_ADDRESS, RELAY0_DI_STATE_ADDRESS, RELAY1_COIL_ADDRESS, RELAY1_DI_STATE_ADDRESS,
        RELAY2_COIL_ADDRESS, RELAY2_DI_STATE_ADDRESS, RELAY3_COIL_ADDRESS, RELAY3_DI_STATE_ADDRESS,
        USB_COIL_ADDRESS, USB_DI_STATE_ADDRESS, USB_IR_CURRENT_ADDRESS, USB_IR_VOLTAGE_ADDRESS,
    };

    #[shared]
    struct Shared {
        state: BoardState,

        rel1: Relay<PinRelay0, PinButton1>,
        rel2: Relay<PinRelay1, PinButton2>,
        rel3: Relay<PinRelay2, PinButton3>,
        rel4: Relay<PinRelay3, PinButton4>,
        high_v_on_off: Relay<PinHighVoltageOnOff, PinButton7>,
        usb_on_off: Relay<PinUsbOnOff, PinButton6>,

        ui: Ui<PinButton5>,

        in_v_filter: AdcChannel,
        j9_c_filter: AdcChannel,
        usb_c_filter: AdcChannel,
        usb_dev: Usb,
        usb_uartm: usbd_serial::SerialPort<'static, hal::usb::UsbBus>,

        modbus: Modbus,

        uart1_rx: Option<rp2040_hal::uart::Reader<UART0, crate::board::Uart0Pins>>,
        uart1_tx: Option<rp2040_hal::uart::Writer<UART0, crate::board::Uart0Pins>>,
        uart2_rx: Option<rp2040_hal::uart::Reader<UART1, crate::board::Uart1Pins>>,
        uart2_tx: Option<rp2040_hal::uart::Writer<UART1, crate::board::Uart1Pins>>,

        usb_uart1_lc: Option<crate::UsbUartParams>,
        usb_uart2_lc: Option<crate::UsbUartParams>,
    }

    #[monotonic(binds = TIMER_IRQ_0, default = true)]
    type MyMono = Monotonic<Alarm0>;

    #[local]
    struct Local {
        adccombo: crate::measurements::adc_combo::StructAdcMeas,
        pico_led: PinPicoLed,

        uart_to_usb1_prod: Producer<'static, u8, 512>,
        uart_to_usb1_cons: Consumer<'static, u8, 512>,
        uart_to_usb2_prod: Producer<'static, u8, 512>,
        uart_to_usb2_cons: Consumer<'static, u8, 512>,
        usb_to_uart1_prod: Producer<'static, u8, 512>,
        usb_to_uart1_cons: Consumer<'static, u8, 512>,
        usb_to_uart2_prod: Producer<'static, u8, 512>,
        usb_to_uart2_cons: Consumer<'static, u8, 512>,

        usb_uart1: usbd_serial::SerialPort<'static, hal::usb::UsbBus>,
        usb_uart2: usbd_serial::SerialPort<'static, hal::usb::UsbBus>,

        usb_uart1_lc: Option<crate::UsbUartParams>,
        usb_uart2_lc: Option<crate::UsbUartParams>,
        uart_freq: fugit::HertzU32,
    }

    #[init (local = [   uart_to_usb1_buf: Queue<u8, 512> = Queue::new(),
                        uart_to_usb2_buf: Queue<u8, 512> = Queue::new(),
                        usb_to_uart1_buf: Queue<u8, 512> = Queue::new(),
                        usb_to_uart2_buf: Queue<u8, 512> = Queue::new()
                        ]) ]
    fn init(c: init::Context) -> (Shared, Local, init::Monotonics) {
        let mut board = Board::new(c.device);

        let mut timer = board.get_timer();
        let alarm = timer.alarm_0().unwrap();

        let mut modbus = Modbus::new();
        let (uart_to_usb1_prod, uart_to_usb1_cons) = c.local.uart_to_usb1_buf.split();
        let (uart_to_usb2_prod, uart_to_usb2_cons) = c.local.uart_to_usb2_buf.split();
        let (usb_to_uart1_prod, usb_to_uart1_cons) = c.local.usb_to_uart1_buf.split();
        let (usb_to_uart2_prod, usb_to_uart2_cons) = c.local.usb_to_uart2_buf.split();

        let uart1 = board
            .get_uart1()
            .enable(
                UartConfig::new(115200.Hz(), DataBits::Eight, None, StopBits::One),
                board.get_uarts_freq(),
            )
            .unwrap();
        let (mut uart1_rx, uart1_tx) = uart1.split();

        let uart2 = board
            .get_uart2()
            .enable(
                UartConfig::new(115200.Hz(), DataBits::Eight, None, StopBits::One),
                board.get_uarts_freq(),
            )
            .unwrap();

        let (mut uart2_rx, uart2_tx) = uart2.split();

        uart1_rx.enable_rx_interrupt();
        uart2_rx.enable_rx_interrupt();

        modbus.init();

        blink_led::spawn_after(500.millis()).unwrap();
        processRelays::spawn().ok();
        update_filters::spawn().ok();
        update_ui::spawn().ok();
        process_ui_button::spawn().ok();
        update_j9::spawn().ok();
        update_usb::spawn().ok();
        process_in_voltage::spawn().ok();
        process_modbus::spawn().ok();
        uart1_tx_handler::spawn().ok();
        uart2_tx_handler::spawn().ok();
        process_usb_mdb::spawn().ok();
        usb_handler::spawn().ok();
        usb_uart_baudrate::spawn().ok();

        let mut usb_dev = Usb::new(board.get_usb_bus());
        let usb_uartm = usb_dev.get_usb_uartm();
        let usb_uart1 = usb_dev.get_usb_uart1();
        let usb_uart2 = usb_dev.get_usb_uart2();

        (
            Shared {
                state: BoardState::new(),
                rel1: Relay::new(board.get_relay0(), board.get_button1()),
                rel2: Relay::new(board.get_relay1(), board.get_button2()),
                rel3: Relay::new(board.get_relay2(), board.get_button3()),
                rel4: Relay::new(board.get_relay3(), board.get_button4()),
                high_v_on_off: Relay::new(board.get_high_v_on_off(), board.get_button7()),
                usb_on_off: Relay::new(board.get_usb_on_off(), board.get_button6()),
                ui: Ui::new(board.get_display(), board.get_button5()),

                in_v_filter: AdcChannel::new(AdcChannels::HighV, 125.0),
                j9_c_filter: AdcChannel::new(AdcChannels::HighCur, 650.0),
                usb_c_filter: AdcChannel::new(AdcChannels::UsbCur, 650.0),
                usb_dev,
                usb_uartm,

                modbus,

                uart1_rx: Some(uart1_rx),
                uart1_tx: Some(uart1_tx),
                uart2_rx: Some(uart2_rx),
                uart2_tx: Some(uart2_tx),
                usb_uart1_lc: None,
                usb_uart2_lc: None,
            },
            Local {
                adccombo: board.get_adc_combo(),
                pico_led: board.get_pico_led(),

                uart_to_usb1_prod,
                uart_to_usb1_cons,
                uart_to_usb2_prod,
                uart_to_usb2_cons,
                usb_to_uart1_prod,
                usb_to_uart1_cons,
                usb_to_uart2_prod,
                usb_to_uart2_cons,

                usb_uart1,
                usb_uart2,
                usb_uart1_lc: None,
                usb_uart2_lc: None,
                uart_freq: board.get_uarts_freq(),
            },
            init::Monotonics(Monotonic::new(timer, alarm)),
        )
    }

    #[task(
        shared = [rel1, rel2, rel3, rel4, high_v_on_off, usb_on_off, state],
        local = [],
    )]
    fn processRelays(mut c: processRelays::Context) {
        let in_acs = c.shared.state.lock(|bs| bs.acs.clone());

        if let Some(cs) = c.shared.rel1.lock(|rel| rel.process(&in_acs)) {
            c.shared.state.lock(|bs| bs.acs = cs);
        }
        if let Some(cs) = c.shared.rel2.lock(|rel| rel.process(&in_acs)) {
            c.shared.state.lock(|bs| bs.acs = cs);
        }
        if let Some(cs) = c.shared.rel3.lock(|rel| rel.process(&in_acs)) {
            c.shared.state.lock(|bs| bs.acs = cs);
        }
        if let Some(cs) = c.shared.rel4.lock(|rel| rel.process(&in_acs)) {
            c.shared.state.lock(|bs| bs.acs = cs);
        }
        if let Some(cs) = c.shared.high_v_on_off.lock(|rel| rel.process(&in_acs)) {
            c.shared.state.lock(|bs| bs.acs = cs);
        }
        if let Some(cs) = c.shared.usb_on_off.lock(|rel| rel.process(&in_acs)) {
            c.shared.state.lock(|bs| bs.acs = cs);
        }
        let out_acs = c.shared.state.lock(|bs| bs.acs.clone());
        if in_acs != out_acs {
            println!("acs changed from {:?} to: {:?}", in_acs, out_acs);
            if out_acs == ControlSource::Modbus {
                process_modbus::spawn().ok();
            }
        }
        processRelays::spawn_after(10.millis()).unwrap();
    }

    #[task(
        shared = [state, ui],
        local = [],
    )]
    fn update_ui(mut c: update_ui::Context) {
        c.shared
            .state
            .lock(|state| c.shared.ui.lock(|u| u.flush(state)));

        update_ui::spawn_after(100.millis()).unwrap();
    }

    #[task(
        shared = [ui, state],
        local = [],
    )]
    fn process_ui_button(mut c: process_ui_button::Context) {
        let cur_acs = c.shared.state.lock(|s| s.acs.clone());

        if let Some(new_acs) = c.shared.ui.lock(|u| u.process_button(cur_acs.clone())) {
            c.shared.state.lock(|s| s.acs = new_acs.clone());

            if cur_acs != new_acs {
                println!("acs changed from {:?} -> {:?}", cur_acs, new_acs);
                if new_acs == ControlSource::Modbus {
                    process_modbus::spawn().ok();
                }
            }
        };
        process_ui_button::spawn_after(10.millis()).unwrap();
    }

    #[task(
        shared = [],
        local = [pico_led, tog: bool = true],
    )]
    fn blink_led(c: blink_led::Context) {
        if *c.local.tog {
            c.local.pico_led.set_high().unwrap();
        } else {
            c.local.pico_led.set_low().unwrap();
        }
        *c.local.tog = !*c.local.tog;

        blink_led::spawn_after(1000.millis()).unwrap();
    }

    #[task(
        shared = [in_v_filter, j9_c_filter, usb_c_filter],
        local = [adccombo],
    )]
    fn update_filters(mut c: update_filters::Context) {
        c.shared.in_v_filter.lock(|f| f.process(c.local.adccombo));
        c.shared.j9_c_filter.lock(|f| f.process(c.local.adccombo));
        c.shared.usb_c_filter.lock(|f| f.process(c.local.adccombo));

        update_filters::spawn_after(20.millis()).unwrap();
    }

    #[task(
        shared = [state, in_v_filter, j9_c_filter, high_v_on_off],
        local = [],
    )]
    fn update_j9(mut c: update_j9::Context) {
        let is_on = c.shared.high_v_on_off.lock(|s| s.get_state());

        c.shared.state.lock(|s| {
            if is_on | crate::SHOW_VALUES_WHEN_OFF {
                s.high_v.voltage = c.shared.in_v_filter.lock(|v| v.read());
                s.high_v.current = c.shared.j9_c_filter.lock(|c| c.read());
            } else if !crate::SHOW_VALUES_WHEN_OFF {
                s.high_v.voltage = 0.0;
                s.high_v.current = 0.0;
            }
            s.high_v.state = is_on;
        });
        update_j9::spawn_after(200.millis()).unwrap();
    }

    #[task(
        shared = [state, usb_c_filter, usb_on_off],
        local = [],
    )]
    fn update_usb(mut c: update_usb::Context) {
        let is_on = c.shared.usb_on_off.lock(|s| s.get_state());

        c.shared.state.lock(|s| {
            if is_on | crate::SHOW_VALUES_WHEN_OFF {
                s.usb.voltage = 5.0;
                s.usb.current = c.shared.usb_c_filter.lock(|c| c.read());
            } else if !crate::SHOW_VALUES_WHEN_OFF {
                s.usb.voltage = 0.0;
                s.usb.current = 0.0;
            }
            s.usb.state = is_on;
        });
        update_usb::spawn_after(200.millis()).unwrap();
    }

    #[task(
        shared = [state, in_v_filter, high_v_on_off, usb_on_off, rel1, rel2, rel3, rel4,],
        local = [],
    )]
    fn process_in_voltage(mut c: process_in_voltage::Context) {
        let in_v = c.shared.in_v_filter.lock(|f| f.read());

        let acs = c.shared.state.lock(|s| s.acs.clone());
        c.shared.state.lock(|s| {
            s.in_voltage_ok = in_v > crate::IN_VOLTAGE_MINIMUM;
            s.in_voltage = in_v;

            if !s.in_voltage_ok {
                // reset due to error
                c.shared
                    .high_v_on_off
                    .lock(|r| r.set_state(false, s.acs.clone(), &acs));
                c.shared
                    .usb_on_off
                    .lock(|r| r.set_state(false, s.acs.clone(), &acs));
                c.shared
                    .rel1
                    .lock(|r| r.set_state(false, s.acs.clone(), &acs));
                c.shared
                    .rel2
                    .lock(|r| r.set_state(false, s.acs.clone(), &acs));
                c.shared
                    .rel3
                    .lock(|r| r.set_state(false, s.acs.clone(), &acs));
                c.shared
                    .rel4
                    .lock(|r| r.set_state(false, s.acs.clone(), &acs));
            }
        });
        process_in_voltage::spawn_after(20.millis()).unwrap();
    }

    #[task(priority = 1, local = [uart_freq],
        shared = [uart1_rx, uart1_tx, uart2_rx, uart2_tx, usb_uart1_lc, usb_uart2_lc])]
    fn usb_uart_baudrate(ctx: usb_uart_baudrate::Context) {
        (
            ctx.shared.uart1_rx,
            ctx.shared.uart1_tx,
            ctx.shared.usb_uart1_lc,
        )
            .lock(|uart1_rx, uart1_tx, lc| {
                if lc.is_some() {
                    println!("params changed");

                    let rx = uart1_rx.take().unwrap();
                    let tx = uart1_tx.take().unwrap();

                    let uart1 = UartPeripheral::join(rx, tx);

                    let uart1 = uart1.disable();
                    let uart1 = uart1
                        .enable(lc.unwrap().to_uart_params(), *ctx.local.uart_freq)
                        .unwrap();

                    let (rx, tx) = uart1.split();
                    uart1_rx.replace(rx);
                    uart1_tx.replace(tx);

                    *lc = None;
                }
            });

        (
            ctx.shared.uart2_rx,
            ctx.shared.uart2_tx,
            ctx.shared.usb_uart2_lc,
        )
            .lock(|uart2_rx, uart2_tx, lc| {
                if lc.is_some() {
                    println!("params changed");

                    let rx = uart2_rx.take().unwrap();
                    let tx = uart2_tx.take().unwrap();

                    let uart1 = UartPeripheral::join(rx, tx);

                    let (rx, tx) = uart1.split();
                    uart2_rx.replace(rx);
                    uart2_tx.replace(tx);

                    *lc = None;
                }
            });

        usb_uart_baudrate::spawn_after(200.micros()).ok();
    }

    #[task(priority = 2, local = [uart_to_usb1_cons, usb_to_uart1_prod, uart_to_usb2_cons, usb_to_uart2_prod, usb_uart1, usb_uart2, usb_uart1_lc, usb_uart2_lc],
        shared = [usb_dev, usb_uartm, usb_uart1_lc, usb_uart2_lc])]
    fn usb_handler(mut ctx: usb_handler::Context) {
        let lc1: crate::UsbUartParams = crate::UsbUartParams::new(
            ctx.local.usb_uart1.line_coding().stop_bits(),
            ctx.local.usb_uart1.line_coding().data_bits(),
            ctx.local.usb_uart1.line_coding().parity_type(),
            ctx.local.usb_uart1.line_coding().data_rate(),
        );

        let lc2: crate::UsbUartParams = crate::UsbUartParams::new(
            ctx.local.usb_uart2.line_coding().stop_bits(),
            ctx.local.usb_uart2.line_coding().data_bits(),
            ctx.local.usb_uart2.line_coding().parity_type(),
            ctx.local.usb_uart2.line_coding().data_rate(),
        );

        if ctx.local.usb_uart1_lc.is_none() {
            ctx.local.usb_uart1_lc.replace(lc1);
            ctx.shared.usb_uart1_lc.lock(|lc| lc.replace(lc1));
        } else if (*ctx.local.usb_uart1_lc).unwrap() != lc1 {
            *ctx.local.usb_uart1_lc = Some(lc1);
            ctx.shared.usb_uart1_lc.lock(|lc| *lc = Some(lc1));
        }

        if ctx.local.usb_uart2_lc.is_none() {
            ctx.local.usb_uart2_lc.replace(lc2);
            ctx.shared.usb_uart2_lc.lock(|lc| lc.replace(lc2));
        } else if (*ctx.local.usb_uart2_lc).unwrap() != lc2 {
            *ctx.local.usb_uart2_lc = Some(lc2);
            ctx.shared.usb_uart2_lc.lock(|lc| *lc = Some(lc2));
        }

        _ = (ctx.shared.usb_dev, ctx.shared.usb_uartm)
            .lock(|usbd, uartm| usbd.process_usb(uartm, ctx.local.usb_uart1, ctx.local.usb_uart2));

        usb_uart_tx_handler(ctx.local.uart_to_usb1_cons, ctx.local.usb_uart1);
        usb_uart_rx_handler(ctx.local.usb_to_uart1_prod, ctx.local.usb_uart1);
        usb_uart_tx_handler(ctx.local.uart_to_usb2_cons, ctx.local.usb_uart2);
        usb_uart_rx_handler(ctx.local.usb_to_uart2_prod, ctx.local.usb_uart2);

        // ctx.local.usb_uart2.line_coding()

        usb_handler::spawn_after(10.micros()).ok();
    }

    #[task(
        shared = [modbus, usb_uartm],
        local = [],
    )]
    fn process_usb_mdb(c: process_usb_mdb::Context) {
        (c.shared.modbus, c.shared.usb_uartm).lock(|mdb, uart| {
            let mut mem_to_send = alloc_stack!([u8; 512]);
            let mut tosend = FixedVec::new(&mut mem_to_send);
            let mut received = [0; 256];

            match uart.read(&mut received) {
                Err(usb_device::UsbError::WouldBlock) => {
                    // Do nothing
                    // println!("process_usb_mdb: receive error");
                }
                Err(_e) => {
                    // Do nothing
                    println!("process_usb_mdb: receive error");
                }
                Ok(0) => {
                    // Do nothing
                }
                Ok(_count) => {
                    mdb.process_data(received, &mut tosend);
                    {
                        // Send back to the host
                        let mut wr_ptr = tosend.as_slice();
                        while !wr_ptr.is_empty() {
                            match uart.write(wr_ptr) {
                                Ok(len) => wr_ptr = &wr_ptr[len..],
                                // On error, just drop unwritten data.
                                // One possible error is Err(WouldBlock), meaning the USB
                                // write buffer is full.
                                Err(_) => break,
                            };
                        }
                    }
                }
            }
        });
        process_usb_mdb::spawn_after(5.millis()).unwrap();
    }

    #[task(
        shared = [modbus, state, usb_dev, rel1, rel2, rel3, rel4, high_v_on_off, usb_on_off, ui ],
        local = [],
    )]
    fn process_modbus(mut c: process_modbus::Context) {
        let acs = c.shared.state.lock(|s| s.acs.clone());

        let state = c.shared.state;
        let rel1 = c.shared.rel1;
        let rel2 = c.shared.rel2;
        let rel3 = c.shared.rel3;
        let rel4 = c.shared.rel4;
        let high_v_on_off = c.shared.high_v_on_off;
        let usb_on_off = c.shared.usb_on_off;
        let ui = c.shared.ui;
        let modbus = c.shared.modbus;

        (
            state,
            rel1,
            rel2,
            rel3,
            rel4,
            high_v_on_off,
            usb_on_off,
            ui,
            modbus,
        )
            .lock(
                |state, rel1, rel2, rel3, rel4, high_v_on_off, usb_on_off, ui, mdb| {
                    let mut process_coil =
                        |relay: &mut dyn crate::relay::IRelay, coil_addr, di_addr| {
                            let relRequest: bool =
                                mdb.process_io(|mdb| mdb.get_coil(coil_addr).unwrap());

                            let relStateAfter =
                                relay.set_state(relRequest, ControlSource::Modbus, &acs);

                            mdb.process_io(|mdb| mdb.set_discrete(di_addr, relStateAfter))
                                .ok();
                        };

                    process_coil(rel1, RELAY0_COIL_ADDRESS, RELAY0_DI_STATE_ADDRESS);
                    process_coil(rel2, RELAY1_COIL_ADDRESS, RELAY1_DI_STATE_ADDRESS);
                    process_coil(rel3, RELAY2_COIL_ADDRESS, RELAY2_DI_STATE_ADDRESS);
                    process_coil(rel4, RELAY3_COIL_ADDRESS, RELAY3_DI_STATE_ADDRESS);
                    process_coil(high_v_on_off, J9_COIL_ADDRESS, J9_DI_STATE_ADDRESS);
                    process_coil(usb_on_off, USB_COIL_ADDRESS, USB_DI_STATE_ADDRESS);

                    let mut process_ir = |ps, ireg_addr| {
                        mdb.process_io(|mdb| mdb.set_input(ireg_addr, ps)).ok();
                    };

                    process_ir(
                        (state.high_v.voltage * VOLTAGE_MULT) as u16,
                        J9_IR_VOLTAGE_ADDRESS,
                    );
                    process_ir(
                        (state.high_v.current * CURRENT_MULT) as u16,
                        J9_IR_CURRENT_ADDRESS,
                    );
                    process_ir(
                        (state.usb.voltage * VOLTAGE_MULT) as u16,
                        USB_IR_VOLTAGE_ADDRESS,
                    );
                    process_ir(
                        (state.usb.current * CURRENT_MULT) as u16,
                        USB_IR_CURRENT_ADDRESS,
                    );
                    process_ir(
                        (state.in_voltage * VOLTAGE_MULT) as u16,
                        DEV_INPUT_VOLTAGE_IR_ADDRESS,
                    );
                    process_ir(ui.get_active_page() as u16, DEV_SCREEN_PAGE_IR_ADDRESS);
                    process_ir(ui.get_page_count() as u16, DEV_SCREEN_COUNT_IR_ADDRESS);

                    let mdb_requested_page = mdb.process_io(|mdb| {
                        mdb.get_holding(DEV_SCREEN_PAGE_HR_ADDRESS).unwrap() as u8
                    });

                    if (state.modbus_requested_page != mdb_requested_page  //req page changed
                    || mdb_requested_page != ui.get_active_page()) // or is different than actual
                    && mdb_requested_page < ui.get_page_count()
                    {
                        state.modbus_requested_page = mdb_requested_page;
                        ui.set_active_page(mdb_requested_page);
                    }

                    let mdb_takeover_request = mdb.process_io(|mdb| {
                        if mdb.get_coil(DEV_ACS_TAKE_COIL_ADDRESS).unwrap() {
                            mdb.set_coil(DEV_ACS_TAKE_COIL_ADDRESS, false).unwrap();
                            mdb.set_input(DEV_ACS_IR_ADDRESS, ControlSource::Modbus as u16)
                                .ok();
                            true
                        } else {
                            mdb.set_input(DEV_ACS_IR_ADDRESS, state.acs.clone() as u16)
                                .ok();
                            false
                        }
                    });
                    if mdb_takeover_request {
                        state.acs = ControlSource::Modbus;
                    }
                },
            );
        process_modbus::spawn_after(10.millis()).ok();
    }

    #[task(binds = UART0_IRQ, priority = 2, local = [uart_to_usb1_prod], shared = [uart1_rx])]
    fn uart1_rx_handler(mut ctx: uart1_rx_handler::Context) {
        ctx.shared.uart1_rx.lock(|uart1_rx| {
            let rx = uart1_rx.as_mut().unwrap();

            uart_rx_handler(ctx.local.uart_to_usb1_prod, rx);
        })
    }

    #[task(binds = UART1_IRQ, priority = 2, local = [uart_to_usb2_prod], shared = [uart2_rx])]
    fn uart2_rx_handler(mut ctx: uart2_rx_handler::Context) {
        ctx.shared.uart2_rx.lock(|uart2_rx| {
            uart_rx_handler(ctx.local.uart_to_usb2_prod, uart2_rx.as_mut().unwrap());
        })
    }

    #[task(priority = 2, local = [usb_to_uart1_cons], shared = [uart1_tx])]
    fn uart1_tx_handler(mut ctx: uart1_tx_handler::Context) {
        ctx.shared.uart1_tx.lock(|uart1_tx| {
            uart_tx_handler(ctx.local.usb_to_uart1_cons, uart1_tx.as_mut().unwrap());
        });
        // 8n1 -> 10b/B
        // 1S/115200b ~ 0.868uS/b * 10b/B >~ 9uS/B,
        // RP2040 UART TX buffer size is 32 bytes,
        // 9uS * 32 = 288uS * 0.8 ~ 230uS.
        uart1_tx_handler::spawn_after(230.micros()).unwrap();
    }

    #[task(priority = 2, local = [usb_to_uart2_cons], shared = [uart2_tx])]
    fn uart2_tx_handler(mut ctx: uart2_tx_handler::Context) {
        ctx.shared.uart2_tx.lock(|uart2_tx| {
            uart_tx_handler(ctx.local.usb_to_uart2_cons, uart2_tx.as_mut().unwrap());
        });
        uart2_tx_handler::spawn_after(230.micros()).unwrap();
    }
}
