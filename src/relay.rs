use embedded_hal::digital::v2::{InputPin, OutputPin, PinState, ToggleableOutputPin};

use crate::state::ControlSource;

use crate::button::{Button, ButtonEvent};

const BUTTON_ACTIVE_LEVEL: bool = false;

pub trait IRelay {
    fn process(&mut self, acs: &ControlSource) -> Option<ControlSource>;

    fn get_state(&self) -> bool;

    fn set_state(&mut self, on_off: bool, cs: ControlSource, acs: &ControlSource) -> bool;
}

pub struct Relay<R, B: InputPin> {
    relay: R,
    button: Button<B>,
    relay_state: bool,
}

impl<R: ToggleableOutputPin + OutputPin, B: InputPin> Relay<R, B> {
    pub fn new(relay_pin: R, button_pin: B) -> Relay<R, B> {
        let mut relay_pin = relay_pin;
        relay_pin.set_low().ok();

        Relay {
            relay: relay_pin,
            button: Button::new(button_pin, 0, 0, BUTTON_ACTIVE_LEVEL),
            relay_state: false,
        }
    }
}

impl<R: ToggleableOutputPin + OutputPin, B: InputPin> IRelay for Relay<R, B> {
    fn process(&mut self, acs: &ControlSource) -> Option<ControlSource> {
        let mut cs = None;

        if let ButtonEvent::PressedDown = self.button.process() {
            cs = Some(ControlSource::Manual);
            self.relay_state = !self.relay_state;

            self.set_state(self.relay_state, ControlSource::Manual, acs);
        }
        cs
    }

    fn get_state(&self) -> bool {
        self.relay_state
    }

    fn set_state(&mut self, on_off: bool, cs: ControlSource, acs: &ControlSource) -> bool {
        if cs == *acs || cs == ControlSource::Manual {
            self.relay.set_state(PinState::from(on_off)).ok();
            self.relay_state = on_off;
        }
        self.relay_state
    }
}
