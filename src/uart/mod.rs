use defmt::println;
use rp_pico::hal;

pub fn uart_rx_handler<D, P>(
    producer: &mut heapless::spsc::Producer<u8, 512>,
    reader: &mut rp2040_hal::uart::Reader<D, P>,
) where
    D: hal::uart::UartDevice,
    P: hal::uart::ValidUartPinout<D>,
{
    let mut bytes: [u8; 32] = [0; 32];
    if producer.len() < 384 {
        while let Ok(size) = reader.read_raw(&mut bytes) {
            let mut r = 0;
            while size > r {
                producer.enqueue(bytes[r]).unwrap();
                r += 1;
            }
        }
    } else {
        println!("skipped");
    }
}

pub fn uart_tx_handler<D, P>(
    consumer: &mut heapless::spsc::Consumer<u8, 512>,
    writer: &mut rp2040_hal::uart::Writer<D, P>,
) where
    D: hal::uart::UartDevice,
    P: hal::uart::ValidUartPinout<D>,
{
    while let Some(data) = consumer.peek() {
        let d: [u8; 1] = [*data];
        if let Err(_would_block) = writer.write_raw(&d as &[u8]) {
            break;
        } else {
            _ = consumer.dequeue();
        }
    }
}
