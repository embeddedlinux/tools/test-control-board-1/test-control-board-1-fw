use embedded_hal::digital::v2::InputPin;

pub enum ButtonEvent {
    PressedLong,
    PressedShort,
    PressedDown,
    ReleasedUp,
    IsUp,
    IsDown,
}

pub struct Button<B>
where
    B: InputPin,
{
    button: B,
    was_button_up_after_down: bool,
    time_counter: u16,
    short_pressed_max: u16,
    long_pressed_min: u16,
    button_down_state: bool,
}

impl<B: InputPin> Button<B> {
    pub fn new(
        button: B,
        short_pressed_max: u16,
        long_pressed_min: u16,
        button_down_state: bool,
    ) -> Button<B> {
        Button {
            button,
            was_button_up_after_down: true,
            time_counter: 0,
            short_pressed_max,
            long_pressed_min,
            button_down_state,
        }
    }

    pub fn process(&mut self) -> ButtonEvent {
        let button_pressed = self.button.is_high().ok().unwrap() == self.button_down_state;

        if button_pressed {
            if self.was_button_up_after_down {
                // just pressed
                self.was_button_up_after_down = false;
                self.time_counter += 1;
                ButtonEvent::PressedDown
            } else {
                self.time_counter += 1;
                if self.time_counter == self.long_pressed_min {
                    return ButtonEvent::PressedLong;
                }
                ButtonEvent::IsDown
            }
        } else {
            if !self.was_button_up_after_down {
                // just released
                self.was_button_up_after_down = true;

                if self.time_counter <= self.short_pressed_max {
                    self.time_counter = 0;
                    return ButtonEvent::PressedShort;
                }
                self.time_counter = 0;
                return ButtonEvent::ReleasedUp;
            }
            ButtonEvent::IsUp
        }
    }

    #[allow(dead_code)]
    pub fn percentage_of_long(&self) -> u16 {
        if self.is_after_short() {
            return (100 * self.time_counter - self.short_pressed_max) / self.long_pressed_min;
        }
        0
    }

    #[allow(dead_code)]
    pub fn is_after_short(&self) -> bool {
        self.short_pressed_max < self.time_counter
    }
}
