use core::str;

use defmt::println;
use embedded_graphics::{
    geometry::Point,
    mono_font::{iso_8859_14::FONT_8X13, iso_8859_2::FONT_6X10, MonoTextStyleBuilder},
    pixelcolor::BinaryColor,
    prelude::*,
    primitives::Line,
    primitives::PrimitiveStyle,
    text::{Baseline, Text},
};
use embedded_hal::digital::v2::InputPin;

use crate::swversion;

use crate::button::{Button, ButtonEvent};

use core::fmt::Write;
use defmt_rtt as _;

// The display driver:
use ssd1306::prelude::*;

use crate::state::ControlSource;

const BUTTON_ACTIVE_LEVEL: bool = false;

const ON_STRING: &str = "On";
const OFF_STRING: &str = "Off";

const PAGES_COUNT: u8 = 4;

const SCREEN_WIDTH: u16 = 128;
const SCREEN_HEIGTH: u16 = 32;

const SHORT_PRESSED_MAX: u16 = 20;
const LONG_PRESSED_MIN: u16 = 50;

type BoardI2c = rp_pico::hal::I2C<
    rp_pico::hal::pac::I2C1,
    (
        rp_pico::hal::gpio::Pin<
            rp_pico::hal::gpio::bank0::Gpio18,
            rp_pico::hal::gpio::Function<rp_pico::hal::gpio::I2C>,
        >,
        rp_pico::hal::gpio::Pin<
            rp_pico::hal::gpio::bank0::Gpio19,
            rp_pico::hal::gpio::Function<rp_pico::hal::gpio::I2C>,
        >,
    ),
>;

// trait ScreenPage {
//     fn draw(&self, state: &crate::state::BoardState);
// }

// struct ScreenPageJ9 {}

// impl ScreenPageJ9 {
//     pub fn new() -> Self {
//         ScreenPageJ9 {}
//     }
//     fn draw(&mut self, state: &crate::state::BoardState) {
//         let mut buf = FmtBuf::new();
//         write!(&mut buf, "J9 {}", self.state2string(state.high_v.state)).unwrap();
//         self.write_line(&mut buf, 0, 0);

//         self.write_acs_inv(state);

//         write!(&mut buf, "{:2.1}V ", state.high_v.voltage).unwrap();
//         self.write_line(&mut buf, 1, 0);

//         write!(&mut buf, "{:.2}A", state.high_v.current).unwrap();
//         self.write_line(&mut buf, 1, 64);
//     }
// }

pub struct Ui<B: InputPin> {
    display: ssd1306::Ssd1306<
        I2CInterface<BoardI2c>,
        ssd1306::size::DisplaySize128x32,
        ssd1306::mode::BufferedGraphicsMode<ssd1306::size::DisplaySize128x32>,
    >,
    page: u8,
    page_count: u8,
    button: Button<B>,
}

impl<B: InputPin> Ui<B> {
    pub fn new(
        mut display: ssd1306::Ssd1306<
            I2CInterface<BoardI2c>,
            ssd1306::size::DisplaySize128x32,
            ssd1306::mode::BufferedGraphicsMode<ssd1306::size::DisplaySize128x32>,
        >,
        button: B,
    ) -> Ui<B> {
        display.init().unwrap();
        Ui {
            display,
            page: 0,
            button: Button::new(
                button,
                SHORT_PRESSED_MAX,
                LONG_PRESSED_MIN,
                BUTTON_ACTIVE_LEVEL,
            ),
            page_count: PAGES_COUNT,
        }
    }

    pub fn process_button(&mut self, acs: ControlSource) -> Option<ControlSource> {
        let mut cs_requested = None;

        match self.button.process() {
            ButtonEvent::PressedShort => {
                self.button_pressed_short();
                println!("PressedShort")
            }
            ButtonEvent::PressedLong => {
                self.button_pressed_long(&acs, &mut cs_requested);
                println!("PressedLong")
            }
            _ => {}
        }
        cs_requested
    }

    fn button_pressed_short(&mut self) {
        self.page += 1;

        if self.page >= PAGES_COUNT {
            self.page = 0;
        }
        println!("self.page={}", self.page);
    }

    fn button_pressed_long(
        &mut self,
        acs: &ControlSource,
        cs_requested: &mut Option<ControlSource>,
    ) {
        if *acs == ControlSource::Manual {
            *cs_requested = Some(ControlSource::Modbus);
        } else {
            *cs_requested = Some(ControlSource::Manual);
        }
    }

    pub fn flush(&mut self, state: &crate::state::BoardState) {
        self.display.clear(BinaryColor::Off).unwrap();

        match self.page {
            0 => self.write_page_j9(state),
            1 => self.write_page_usb(state),
            2 => self.write_page_about(state),
            3 => self.write_page_all(state),
            _ => {} //self.page = 0,
        }
        self.draw_menu_position();
        self.write_progress_line();

        self.display.flush().unwrap();
    }

    fn state2string<'a>(&self, state: bool) -> &'a str {
        match state {
            true => ON_STRING,
            false => OFF_STRING,
        }
    }

    fn draw_menu_position(&mut self) {
        let line_length = SCREEN_HEIGTH / (PAGES_COUNT as u16);
        let line_position = line_length * (self.page as u16);

        Line::new(
            Point::new((SCREEN_WIDTH - 1) as i32, line_position as i32),
            Point::new(
                (SCREEN_WIDTH - 1) as i32,
                (line_position + line_length) as i32,
            ),
        )
        .into_styled(PrimitiveStyle::with_stroke(BinaryColor::On, 1))
        .draw(&mut self.display)
        .ok();
    }
    fn write_progress_line(&mut self) {
        if self.button.is_after_short() {
            let length = (SCREEN_WIDTH * self.button.percentage_of_long()) / 100;
            Line::new(
                Point::new(0, (SCREEN_HEIGTH - 1) as i32),
                Point::new(length as i32, (SCREEN_HEIGTH - 1) as i32),
            )
            .into_styled(PrimitiveStyle::with_stroke(BinaryColor::On, 1))
            .draw(&mut self.display)
            .ok();
        }
    }

    fn write_line(&mut self, buf: &mut FmtBuf, line: u8, horizontal: u8) {
        let text_style = MonoTextStyleBuilder::new()
            .font(&FONT_8X13)
            .text_color(BinaryColor::On)
            .build();

        let py = 15 * line;

        Text::with_baseline(
            buf.as_str(),
            Point::new(horizontal.into(), py.into()),
            text_style,
            Baseline::Top,
        )
        .draw(&mut self.display)
        .unwrap();
        buf.reset();
    }

    fn acs_to_char(&self, cs: &ControlSource) -> char {
        if *cs == ControlSource::Manual {
            'L'
        } else {
            'R'
        }
    }

    fn get_power_supply(&self, state: &crate::state::BoardState) -> char {
        if state.in_voltage_ok {
            '*'
        } else {
            '_'
        }
    }

    fn write_acs_inv(&mut self, state: &crate::state::BoardState) {
        let mut buf = FmtBuf::new();
        write!(
            &mut buf,
            "{} {}",
            self.acs_to_char(&state.acs),
            self.get_power_supply(state)
        )
        .unwrap();
        self.write_line(&mut buf, 0, 100);
    }

    pub fn set_active_page(&mut self, _page: u8) {
        // self.page = page;
        // println!("self.page={}", self.page);
    }

    pub fn get_active_page(&self) -> u8 {
        self.page
    }

    pub fn get_page_count(&self) -> u8 {
        self.page_count
    }

    fn write_page_j9(&mut self, state: &crate::state::BoardState) {
        let mut buf = FmtBuf::new();
        write!(&mut buf, "J9 {}", self.state2string(state.high_v.state)).unwrap();
        self.write_line(&mut buf, 0, 0);

        self.write_acs_inv(state);

        write!(&mut buf, "{:2.1}V ", state.high_v.voltage).unwrap();
        self.write_line(&mut buf, 1, 0);

        write!(&mut buf, "{:.2}A", state.high_v.current).unwrap();
        self.write_line(&mut buf, 1, 64);
    }

    fn write_page_usb(&mut self, state: &crate::state::BoardState) {
        let mut buf = FmtBuf::new();

        write!(&mut buf, "USB {}", self.state2string(state.usb.state)).unwrap();
        self.write_line(&mut buf, 0, 0);

        self.write_acs_inv(state);

        write!(&mut buf, "{:2.1}V ", state.usb.voltage).unwrap();
        self.write_line(&mut buf, 1, 0);

        write!(&mut buf, "{:.2}A", state.usb.current).unwrap();
        self.write_line(&mut buf, 1, 64);
    }

    fn write_page_about(&mut self, state: &crate::state::BoardState) {
        let mut buf = FmtBuf::new();

        write!(&mut buf, "ver:{}", swversion::get_version()).unwrap();
        self.write_line(&mut buf, 0, 0);

        self.write_acs_inv(state);

        write!(&mut buf, "IN: {:2.1}V ", state.in_voltage).unwrap();
        self.write_line(&mut buf, 1, 0);
    }

    fn write_page_all(&mut self, state: &crate::state::BoardState) {
        let text_style = MonoTextStyleBuilder::new()
            .font(&FONT_6X10)
            .text_color(BinaryColor::On)
            .build();

        let mut buf = FmtBuf::new();

        //First line
        self.write_acs_inv(state);

        write!(&mut buf, "In {:.2}V", state.in_voltage).unwrap();
        Text::with_baseline(buf.as_str(), Point::zero(), text_style, Baseline::Top)
            .draw(&mut self.display)
            .unwrap();
        buf.reset();

        //Second Line
        write!(&mut buf, "J9 {:.2}V,", state.high_v.voltage).unwrap();
        Text::with_baseline(buf.as_str(), Point::new(0, 11), text_style, Baseline::Top)
            .draw(&mut self.display)
            .unwrap();
        buf.reset();

        write!(&mut buf, "{:.2}A", state.high_v.current).unwrap();
        Text::with_baseline(buf.as_str(), Point::new(64, 11), text_style, Baseline::Top)
            .draw(&mut self.display)
            .unwrap();
        buf.reset();

        //Third Line

        write!(&mut buf, "USB {:.2}V", state.usb.voltage).unwrap();
        Text::with_baseline(buf.as_str(), Point::new(0, 22), text_style, Baseline::Top)
            .draw(&mut self.display)
            .unwrap();
        buf.reset();
        write!(&mut buf, "{:.2}A", state.usb.current).unwrap();
        Text::with_baseline(buf.as_str(), Point::new(64, 22), text_style, Baseline::Top)
            .draw(&mut self.display)
            .unwrap();
        buf.reset();
    }
}

/// This is a very simple buffer to pre format a short line of text
/// limited arbitrarily to 64 bytes.
struct FmtBuf {
    buf: [u8; 64],
    ptr: usize,
}

impl FmtBuf {
    fn new() -> Self {
        Self {
            buf: [0; 64],
            ptr: 0,
        }
    }

    fn reset(&mut self) {
        self.ptr = 0;
    }

    fn as_str(&self) -> &str {
        core::str::from_utf8(&self.buf[0..self.ptr]).unwrap()
    }
}

impl core::fmt::Write for FmtBuf {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        let rest_len = self.buf.len() - self.ptr;
        let len = if rest_len < s.len() {
            rest_len
        } else {
            s.len()
        };
        self.buf[self.ptr..(self.ptr + len)].copy_from_slice(&s.as_bytes()[0..len]);
        self.ptr += len;
        Ok(())
    }
}
