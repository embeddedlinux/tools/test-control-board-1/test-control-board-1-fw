use fixedvec::FixedVec;

use rmodbus::{server::ModbusFrame, ModbusProto};

use rmodbus::server::context::ModbusContextSmall;

use crate::swversion;

pub const RELAY0_COIL_ADDRESS: u16 = 10;
pub const RELAY0_DI_STATE_ADDRESS: u16 = 11;

pub const RELAY1_COIL_ADDRESS: u16 = 20;
pub const RELAY1_DI_STATE_ADDRESS: u16 = 21;

pub const RELAY2_COIL_ADDRESS: u16 = 30;
pub const RELAY2_DI_STATE_ADDRESS: u16 = 31;

pub const RELAY3_COIL_ADDRESS: u16 = 40;
pub const RELAY3_DI_STATE_ADDRESS: u16 = 41;

pub const J9_COIL_ADDRESS: u16 = 100;
pub const J9_DI_STATE_ADDRESS: u16 = 101;

pub const J9_IR_VOLTAGE_ADDRESS: u16 = 102;
pub const J9_IR_CURRENT_ADDRESS: u16 = 103;

pub const USB_COIL_ADDRESS: u16 = 200;
pub const USB_DI_STATE_ADDRESS: u16 = 201;

pub const USB_IR_VOLTAGE_ADDRESS: u16 = 202;
pub const USB_IR_CURRENT_ADDRESS: u16 = 203;

pub const DEV_INPUT_VOLTAGE_IR_ADDRESS: u16 = 300;

pub const DEV_FW_VER_MAJOR_IR_ADDRESS: u16 = 400;
pub const DEV_FW_VER_MINOR_IR_ADDRESS: u16 = 401;
pub const DEV_FW_VER_PATCH_IR_ADDRESS: u16 = 402;

pub const DEV_SCREEN_PAGE_HR_ADDRESS: u16 = 500;
pub const DEV_SCREEN_PAGE_IR_ADDRESS: u16 = 501;
pub const DEV_SCREEN_COUNT_IR_ADDRESS: u16 = 502;

pub const DEV_ACS_TAKE_COIL_ADDRESS: u16 = 510;
pub const DEV_ACS_IR_ADDRESS: u16 = 511;

pub struct Modbus {
    stack_context: ModbusContextSmall,
}
impl Modbus {
    pub fn new() -> Self {
        Modbus {
            stack_context: ModbusContextSmall::new(),
        }
    }

    pub fn init(&mut self) {
        self.stack_context
            .set_input(DEV_FW_VER_MAJOR_IR_ADDRESS, swversion::get_major())
            .ok();

        self.stack_context
            .set_input(DEV_FW_VER_MINOR_IR_ADDRESS, swversion::get_minor())
            .ok();

        self.stack_context
            .set_input(DEV_FW_VER_PATCH_IR_ADDRESS, swversion::get_patch())
            .ok();
    }

    pub fn process_data(&mut self, data_in: [u8; 256], data_out: &mut FixedVec<u8>) {
        let mut frame = ModbusFrame::new(1, &data_in, ModbusProto::Rtu, data_out);
        if frame.parse().is_err() {
            // println!("server error");
            return;
        }
        if frame.processing_required {
            let result = match frame.readonly {
                true => frame.process_read(&self.stack_context),
                false => frame.process_write(&mut self.stack_context),
            };
            if result.is_err() {
                // println!("frame processing error");
                return;
            }
        }

        if frame.response_required {
            frame.finalize_response().unwrap();
        }
    }

    pub fn process_io<P, T>(&mut self, f: P) -> T
    where
        P: Fn(&mut ModbusContextSmall) -> T,
    {
        f(&mut self.stack_context)
    }
}

impl Default for Modbus {
    fn default() -> Self {
        Self::new()
    }
}
